# limesurvey
Please check out the /docs directory for further information.

# LOUD add-ons

## Get survey JSON
#### for testing
To get raw JSON string containing survey structure and options, visit 
http://localhost:4080/survey/index.php/admin/surveylist/getsurveyjson/surveyid/SURVEY_ID

example:

http://localhost:4080/survey/index.php/admin/surveylist/getsurveyjson/surveyid/837878

Data format is:
```javascript
{
    "id":"837878", // limesurvey survey ID
    "title":"test1", // title
    "description":"test1 description", // description
    "intro":"test1 welcome message", // intor text
    "outro":"test1 end message", // outro text
    "questions": // array of question objects
    [
        {
            "id":"1", // id of question. use this when sending survey back
            "original_type":"L", // safe to ignore, internal data
            "qroup_id":"1", // safe to ignore, internal data
            "mandatory":false, // wether this is mandatory to fill in
            "multiple":false, // is it allowed to select multiple answers
            "code":"X1X1", // safe to ignore, internal data
            "text":"choose one", // question text
            "options": // array of available answers - options
            {
                "A1":"qwe", // option key => text
                "A2":"asd", // option key => text
                "A3":"yxc" // option key => text
            },
            "type":"list" // type of question
        },
        {
            "id":"3",
            "original_type":"Y",
            "qroup_id":"3",
            "mandatory":true,
            "multiple":false,
            "code":"X1X3",
            "text":"yes\/no?",
            "options":
            {
                "Y":"Yes",
                "N":"No"
            },
            "type":"yesno"
            }
        },
        {
            "id":"10",
            "original_type":"M",
            "qroup_id":"10",
            "mandatory":true,
            "multiple":true,
            "code":"X3X10",
            "text":"asd asd asd a",
            "options":
            {
                "aa001":"aa",
                "aa002":"bb",
                "aa003":"cc"
            },
            "type":"list"
        }
        // more questions here...
    ]
}
```

## Push survey data to redis
To push survey data to redis, view the desired survey and click "Start Survey" button at the bottom.

Data is published to loudEventChannel channel. Format is:
```javascript
[
	'event' => 'VOTING_STARTED',
	'id' => $surveyID, // limesurvey survey ID
	'data' => $surveyData // survey data, see above
];
```

## Close survey and publish to redis
To close the survey and push event to redis, view the desired survey and click "Close Survey" button at the bottom.

Data is published to loudEventChannel channel. Format is:
```javascript
[
	'event' => 'VOTING_ENDED',
	'id' => $surveyID, // limesurvey survey ID
];
```



## send filled-in survey
When user submits/answers the survey questions, send data to 
http://localhost:4080/survey/index.php/admin/surveylist/savesurvey/surveyid/SURVEY_ID

example:
http://localhost:4080/survey/index.php/admin/surveylist/savesurvey/surveyid/837878

data to be sent must follow the following rules:

 * do a POST request to survey URL
 * for each question, prefix question ID with 'q_', that is the data key
 * in case of a question having multiple => false,
     * value is the key of the option
 * in case the question has multiple => true
     * value is an array
     * each key is the key of the option
     * in case the option was selected, set that value to 'Y'
  
To ilustrate the multiple-choice case, let's say question data is 
```javascript
{
    "id":"1", 
    "mandatory":true, 
    "multiple":true, 
    "text":"choose all that apply", 
    "options": 
    {
        "KEY1":"option 1 text", 
        "KEY2":"option 2 text",
        "KEY3":"option 3 text"
    },
    "type":"list"
}

// if user selected only option2, data sent to server should be
["q_1" => ["KEY2" => "Y"]]
// if user selected option1 and option3, data sent to server should be
["q_1" => ["KEY1" => "Y", "KEY3" => "Y"]]

```

```javascript
// example payload
[
    'q_1' => 'A2', 
    'q_3' => 'Y',
    'q_10' => [
        'aa001' => 'Y', // this question is a multiple-choice, send each key selected
        'aa002' => '' // not selected options can be an empty string
        // or not present at all
    ]
]
```

Server response is JSON
```javascript
{
    "success":true,
    "stored_id":"20"
}
```

## Get survey results

Survey results are available at http://localhost:4080/survey/index.php/admin/surveylist/getsurveyresults/surveyid/SURVEY_ID

Example:
http://localhost:4080/survey/index.php/admin/surveylist/getsurveyresults/surveyid/837878

Format is:
```javascript
{
    "id":"837878",
    "title":"test1",
    "description":"test1 description",
    "intro":"test1 welcome message",
    "outro":"test1 end message",
    "answers":
    [
        {
            "id":"1",
            "text":"choose one",
            "results":
            [
                {
                    "text":"qwe",
                    "code":"A1",
                    "votes":0
                },
                {
                    "text":"asd",
                    "code":"A2",
                    "votes":1
                },
                {
                    "text":"yxc",
                    "code":"A3",
                    "votes":0
                }
            ]
        },
        {
            "id":"3",
            "text":"yes\/no?",
            "results":
            [
                {
                    "text":"Yes",
                    "code":"Y",
                    "votes":1
                },
                {
                    "text":"No",
                    "code":"N",
                    "votes":0
                }
            ]
        },
        {
            "id":"4",
            "text":"check all that apply",
            "results":
            [
                {
                    "text":"odabir1",
                    "code":"SQ001",
                    "votes":1
                },
                {
                    "text":"odabir2",
                    "code":"SQ002",
                    "votes":0
                },
                {
                    "text":"odabir3",
                    "code":"SQ003",
                    "votes":1
                },
                {
                    "text":"odabir4",
                    "code":"SQ004",
                    "votes":0
                }
            ]
        },
        {
            "id":"9",
            "text":"what is your favourite colour?",
            "results":
            [
                {
                    "text":"blue",
                    "code":"mir01",
                    "votes":1
                },
                {
                    "text":"yellow",
                    "code":"mir02",
                    "votes":0
                },
                {
                    "text":"I don't know",
                    "code":"mir03",
                    "votes":0
                }
            ]
        },
        {
            "id":"10",
            "text":"asd asd asd a",
            "results":
            [
                {
                    "text":"aa",
                    "code":"aa001",
                    "votes":0
                },
                {
                    "text":"bb",
                    "code":"aa002",
                    "votes":0
                },
                {
                    "text":"cc",
                    "code":"aa003",
                    "votes":0
                }
            ]
        }
    ],
    "total_answers":5
}
```