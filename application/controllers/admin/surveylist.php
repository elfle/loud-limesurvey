<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    /**
     * LoudApp actions Controller
     *
     *
     * @package       LimeSurvey
     * @subpackage    Backend
     */
    class Surveylist extends Survey_Common_Action
    {

        function __construct($controller, $id)
        {
            parent::__construct($controller, $id);

            Yii::app()->loadHelper('loud');
        }

        protected function enforceAdminLoggedIn()
        {
            if (!Permission::model()->hasGlobalPermission('settings','read')) {
                die();
            }
        }

        /**
         * another override for LOUD case: we need to accept non-authenticated requests
         *
         * @param array $params
         * @return bool
         */
        public function runWithParams($params)
        {
            // Default method that would be called if the subaction and run() do not exist
            $sDefault = 'index';

            // Check for a subaction
            if (empty($params['sa']))
            {
                $sSubAction = $sDefault; // default
            }
            else
            {
                $sSubAction = $params['sa'];
            }

            // Check if the class has the method
            $oClass = new ReflectionClass($this);
            if (!$oClass->hasMethod($sSubAction))
            {
                // If it doesn't, revert to default Yii method, that is run() which should reroute us somewhere else
                $sSubAction = 'run';
            }

            // Populate the params. eg. surveyid -> iSurveyId
            $params = $this->_addPseudoParams($params);

            // LOUD override: only check Permission model for the following set of actions (methods of this class)
            $require_permission_for_actions = ['surveypush', 'surveyend'];

            if (!empty($params['iSurveyId']))
            {
                if(!Survey::model()->findByPk($params['iSurveyId']))
                {
                    $this->getController()->error('Invalid survey id');
                }
                elseif(in_array($params['sa'], $require_permission_for_actions) && !Permission::model()->hasSurveyPermission($params['iSurveyId'], 'survey', 'read'))
                {
                    var_dump($params);
                    $this->getController()->error('No permission');
                }
                else
                {
                    LimeExpressionManager::SetSurveyId($params['iSurveyId']); // must be called early - it clears internal cache if a new survey is being used
                }
            }

            // Check if the method is public and of the action class, not its parents
            // ReflectionClass gets us the methods of the class and parent class
            // If the above method existence check passed, it might not be neceessary that it is of the action class
            $oMethod  = new ReflectionMethod($this, $sSubAction);

            // Get the action classes from the admin controller as the urls necessarily do not equal the class names. Eg. survey -> surveyaction
            $aActions = Yii::app()->getController()->getActionClasses();
            if(empty($aActions[$this->getId()]) || strtolower($oMethod->getDeclaringClass()->name) != $aActions[$this->getId()] || !$oMethod->isPublic())
            {
                // Either action doesn't exist in our whitelist, or the method class doesn't equal the action class or the method isn't public
                // So let us get the last possible default method, ie. index
                $oMethod = new ReflectionMethod($this, $sDefault);
            }

            // We're all good to go, let's execute it
            // runWithParamsInternal would automatically get the parameters of the method and populate them as required with the params
            return parent::runWithParamsInternal($this, $oMethod, $params);
        }

        /**
         * Shows the index page
         *
         * @access public
         * @return void
         */
        public function index()
        {
            die('it lives');

        }

        public function getsurveyjson()
        {

            $iSurveyID = sanitize_int(Yii::app()->request->getParam('surveyid'));
            $json = LoudHelper::getSurveyAsObject($iSurveyID);

            header('Content-Type: application/json');
            echo json_encode($json);
        }
        public function getsurveyprettyjson()
        {

            $iSurveyID = sanitize_int(Yii::app()->request->getParam('surveyid'));
            $json = LoudHelper::getSurveyAsObject($iSurveyID);

            echo str_replace(["\n", "    "], ['<br />', '&nbsp;&nbsp;&nbsp;&nbsp;'], json_encode($json, JSON_PRETTY_PRINT));
        }

        public function getsurveyresults()
        {
            header('Content-Type: application/json');

            $iSurveyID = sanitize_int(Yii::app()->request->getParam('surveyid'));
            $results = LoudHelper::getSurveyAnswers($iSurveyID);

            echo json_encode($results);
        }

        public function testsavesurvey()
        {
            $survey_id = '837878';

            $input = [
                'q_1' => 'A2',
                'q_3' => 'Y',
                'q_4' => [
                    'SQ001' => 'Y',
                    'SQ003' => 'Y'
                ],
                'q_9' => 'mir01'
            ];

            var_dump('test input is', $input);

            return $this->dosavesurvey($survey_id, $input, true);
        }

        public function savesurvey()
        {
            $survey_id = sanitize_int(Yii::app()->request->getParam('surveyid'));

//            $request = Yii::app()->request;
            $request = $_POST; // unsafe and wrong, @todo: fix

            $success = $this->dosavesurvey($survey_id, $request);

            header('Content-Type: application/json');
            echo json_encode([
                'success' => !!$success,
                'stored_id' => $success
            ]);
        }

        protected function dosavesurvey($survey_id, $input, $simulation = false)
        {
            $survey_structure = LoudHelper::getSurveyAsObject($survey_id);

            $survey = SurveyDynamic::model($survey_id);

            // pass through questions, store each response
            foreach($survey_structure->questions as $question)
            {
                $column_prefix = $survey_id . $question->code;
                $input_prefix = 'q_' . $question->id;

                switch($question->type)
                {
                    case 'list':
                        // this is a list, it can be multiple choice or single value
                        // stored in DB differently
                        if($question->multiple)
                        {
                            // expects value as $POST[$input_prefix][$option_code]
                            if(isset($input[$input_prefix]))
                            {
//                                var_dump('here', $question);
                                // one DB column for each option
                                foreach($question->options as $option_code => $option)
                                {
//                                    var_dump('checking', $input_prefix, $option_code);die;
                                    $data[$column_prefix . $option_code] =
                                        isset($input[$input_prefix][$option_code])
                                            ? $input[$input_prefix][$option_code]
                                            : '';
                                }
                            }
                        }
                        else
                        {
                            $data[$column_prefix] = isset($input[$input_prefix]) ? $input[$input_prefix] : '';
                        }
                        break;

                    case 'yesno':
                    case 'gender':
                        $data[$column_prefix] = isset($input[$input_prefix]) ? $input[$input_prefix] : '';
                        break;
                }
            }


            if($simulation)
            {
                var_dump($survey_structure->questions, $data);
                return false;
            }
            else
            {
                $saved_id = $survey->insertRecords($data);
                return $saved_id;
            }
        }

        public function surveypush()
        {
            $iSurveyID = sanitize_int(Yii::app()->request->getParam('surveyid'));
            $surveyData = LoudHelper::getSurveyAsObject($iSurveyID);

            $json = [
                'event' => 'VOTING_STARTED',
                'id' => $iSurveyID,
                'data' => $surveyData
            ];

            $success = LoudHelper::publishToRedis('loudEventChannel', json_encode($json));

            if($success['success'])
            {
                $success['channel'] = 'loudEventChannel';
                $success['sent'] = $json;
            }

            header('Content-Type: application/json');
            echo json_encode($success);
        }

        public function surveyend()
        {
            $iSurveyID = sanitize_int(Yii::app()->request->getParam('surveyid'));

            $json = [
                'event' => 'VOTING_ENDED',
                'id' => $iSurveyID
            ];

            $success = LoudHelper::publishToRedis('loudEventChannel', json_encode($json));

            if($success['success'])
            {
                $success['channel'] = 'loudEventChannel';
                $success['sent'] = $json;
            }

            header('Content-Type: application/json');
            echo json_encode($success);
        }
    }
