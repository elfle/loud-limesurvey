<?php

	class LoudHelper
	{
		public static function getSurveyAsObject($survey_id)
		{
			// ffs globals
			global $quexmllang;

			$result       = new stdClass();
			$survey_model = Survey::model()
								  ->findByPk($survey_id);

			$survey_language = $survey_model->language;
			$quexmllang      = $survey_language;

			$Query = "
SELECT * FROM {{surveys}},{{surveys_languagesettings}}
  WHERE sid = $survey_id
    AND surveyls_survey_id=sid
    AND surveyls_language='" . $survey_language . "'";

			$QueryResult  = Yii::app()->db->createCommand($Query)->query();
			$language_row = $QueryResult->read();


//        var_dump($survey_model);

			// basic info: id, intro, outro, description

			$result->id          = $survey_id;
			$result->title       = $language_row['surveyls_title'];
			$result->description = $language_row['surveyls_description'];
			$result->intro       = $language_row['surveyls_welcometext'];
			$result->outro       = $language_row['surveyls_endtext'];
			$result->questions   = [];


			// get questions
			$Query       = "
SELECT * FROM {{groups}}
  WHERE sid=$survey_id
  AND language='$survey_language'
  ORDER BY group_order ASC
";
			$QueryResult = Yii::app()->db->createCommand($Query)->query();


			$qlang = new limesurvey_lang($survey_model->language);


			//for each section (group)
			foreach ($QueryResult->readAll() as $Row)
			{
				$gid = $Row['gid'];

				// skip group title and description

				//foreach question
				$Query = "
SELECT * FROM {{questions}}
WHERE sid=$survey_id
AND gid = $gid
AND parent_qid=0
AND language='$survey_language'
AND type NOT LIKE 'X'
ORDER BY question_order ASC
";


				$QR = Yii::app()->db->createCommand($Query)->query();
				foreach ($QR->readAll() as $RowQ)
				{
					$question = new stdClass();

//                var_dump($RowQ);

					$question->id            = $RowQ['qid'];
					$question->original_type = $RowQ['type'];
					$question->qroup_id      = $RowQ['qid'];
					$question->mandatory     = $RowQ['mandatory'] == 'Y' ? true : false;
					$question->multiple      = false;
					$question->code          = 'X' . $gid . 'X' . $RowQ['qid'];

					// this might have <br /> -> check with Lovro wether to clean it...
					$question->text = $RowQ['question'];

					$other = false;
					if ($RowQ['other'] == 'Y')
					{
						$other = true;
					}

					// based on type, add more data
					$question->options = [];

					$sgq = $question->code;

					$qid = $question->id;
					switch ($question->original_type)
					{
						case "X": //BOILERPLATE QUESTION - none should appear

							break;
						case "5": //5 POINT CHOICE radio-buttons
							$question->type    = 'list';
							$question->options = ["1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5];
							break;
						case "D": //DATE
							$question->type = 'date';
							break;
						case "L": //LIST drop-down/radio-button list
							$question->type    = 'list';
							$question->options = self::getQuestionAnswersAsArray($qid, false, false, 0, $other, $sgq);
							break;
						case "!": //List - dropdown
							$question->type    = 'list';
							$question->options = self::getQuestionAnswersAsArray($qid, false, false, 0, $other, $sgq);
							break;
						case "O": //LIST WITH COMMENT drop-down/radio-button list + textarea
							/*
							quexml_create_subQuestions($question,$qid,$sgq);
							$response = $dom->createElement("response");
							$response->setAttribute("varName",QueXMLCleanup($sgq));
							$response->appendChild(QueXMLCreateFixed($qid,false,false,0,$other,$sgq));

							$response2 = $dom->createElement("response");
							$response2->setAttribute("varName",QueXMLCleanup($sgq) . "_comment");
							$response2->appendChild(QueXMLCreateFree("longtext","40",""));

							$question->appendChild($response);
							$question->appendChild($response2);
							*/
							break;
						case "R": //RANKING STYLE
							/*
							quexml_create_subQuestions($question,$qid,$sgq,true);
							$Query = "SELECT COUNT(*) as sc FROM {{answers}} WHERE qid = $qid AND language='$quexmllang' ";
							$QRE = Yii::app()->db->createCommand($Query)->query();
							//$QRE = mysql_query($Query) or die ("ERROR: $QRE<br />".mysql_error());
							//$QROW = mysql_fetch_assoc($QRE);
							$QROW = $QRE->read();
							$response->appendChild(QueXMLCreateFree("integer",strlen($QROW['sc']),""));
							$question->appendChild($response);
							*/
							break;
						case "M": //Multiple choice checkbox
							$question->type     = 'list';
							$question->multiple = true;
							$question->options  = self::getSubquestionsAsArray($qid);
//                        $question->options = self::getQuestionAnswersAsArray($qid, false, false, 0, $other, $sgq);
//                        quexml_create_multi($question,$qid,$sgq,false,false,$other);
							break;
						case "P": //Multiple choice with comments checkbox + text
							//Not yet implemented
							quexml_create_multi($question, $qid, $sgq, false, false, $other);
							//no comments added
							break;
						case "Q": //MULTIPLE SHORT TEXT
							/*
							quexml_create_subQuestions($question,$qid,$sgq);
							$response->appendChild(QueXMLCreateFree("text",quexml_get_lengthth($qid,"maximum_chars","10"),""));
							$question->appendChild($response);
							*/
							break;
						case "K": //MULTIPLE NUMERICAL
							/*
							quexml_create_subQuestions($question,$qid,$sgq);
							$response->appendChild(QueXMLCreateFree("integer",quexml_get_lengthth($qid,"maximum_chars","10"),""));
							$question->appendChild($response);
							*/
							break;
						case "N": //NUMERICAL QUESTION TYPE
							/*
							$response->appendChild(QueXMLCreateFree("integer",quexml_get_lengthth($qid,"maximum_chars","10"),""));
							$question->appendChild($response);
							*/
							break;
						case "S": //SHORT FREE TEXT
							$question->type = 'text';
							// default is fieldlength of 24 characters.
//                        $response->appendChild(QueXMLCreateFree("text",quexml_get_lengthth($qid,"maximum_chars","24"),""));
//                        $question->appendChild($response);
							break;
						case "T": //LONG FREE TEXT
							$question->type = 'longtext';
//                        $response->appendChild(QueXMLCreateFree("longtext",quexml_get_lengthth($qid,"display_rows","40"),""));
//                        $question->appendChild($response);
							break;
						case "U": //HUGE FREE TEXT
							$question->type = 'longtext';
//                        $response->appendChild(QueXMLCreateFree("longtext",quexml_get_lengthth($qid,"display_rows","80"),""));
//                        $question->appendChild($response);
							break;
						case "Y": //YES/NO radio-buttons
							$question->type    = 'yesno';
							$question->options = array('Y' => $qlang->gT("Yes"), 'N' => $qlang->gT("No"));

//                        $response->appendChild(QueXMLFixedArray(array($qlang->gT("Yes") => 'Y',$qlang->gT("No") => 'N')));
//                        $question->appendChild($response);
							break;
						case "G": //GENDER drop-down list
							$question->type    = 'gender';
							$question->options = array('F' => $qlang->gT("Female"), 'M' => $qlang->gT("Male"));

//                        $response->appendChild(QueXMLFixedArray(array($qlang->gT("Female") => 'F',$qlang->gT("Male") => 'M')));
//                        $question->appendChild($response);
							break;
						case "A": //ARRAY (5 POINT CHOICE) radio-buttons
							/*
							quexml_create_subQuestions($question,$qid,$sgq);
							$response->appendChild(QueXMLFixedArray(array("1" => 1,"2" => 2,"3" => 3,"4" => 4,"5" => 5)));
							$question->appendChild($response);*/
							break;
						case "B": //ARRAY (10 POINT CHOICE) radio-buttons
							/* quexml_create_subQuestions($question,$qid,$sgq);
								$response->appendChild(QueXMLFixedArray(array("1" => 1,"2" => 2,"3" => 3,"4" => 4,"5" => 5,"6" => 6,"7" => 7,"8" => 8,"9" => 9,"10" => 10)));
								$question->appendChild($response);*/
							break;
						case "C": //ARRAY (YES/UNCERTAIN/NO) radio-buttons
							/* quexml_create_subQuestions($question,$qid,$sgq);
								$response->appendChild(QueXMLFixedArray(array($qlang->gT("Yes") => 'Y',$qlang->gT("Uncertain") => 'U',$qlang->gT("No") => 'N')));
								$question->appendChild($response);*/
							break;
						case "E": //ARRAY (Increase/Same/Decrease) radio-buttons
							/*quexml_create_subQuestions($question,$qid,$sgq);
							$response->appendChild(QueXMLFixedArray(array($qlang->gT("Increase") => 'I',$qlang->gT("Same") => 'S',$qlang->gT("Decrease") => 'D')));
							$question->appendChild($response);*/
							break;
						case "F": //ARRAY (Flexible) - Row Format
							//select subQuestions from answers table where QID
							/* quexml_create_subQuestions($question,$qid,$sgq);
								$response->appendChild(QueXMLCreateFixed($qid,false,false,0,$other,$sgq));
								$question->appendChild($response);*/
							//select fixed responses from
							break;
						case "H": //ARRAY (Flexible) - Column Format
							/*quexml_create_subQuestions($question,$RowQ['qid'],$sgq);
							$response->appendChild(QueXMLCreateFixed($qid,true,false,0,$other,$sgq));
							$question->appendChild($response);*/
							break;
						case "1": //Dualscale multi-flexi array
							/*//select subQuestions from answers table where QID
							quexml_create_subQuestions($question,$qid,$sgq);
							//get the header of the first scale of the dual scale question
							$Query = "SELECT value FROM {{question_attributes}} WHERE qid = $qid AND language='$quexmllang' AND attribute='dualscale_headerA'";
							$QRE = Yii::app()->db->createCommand($Query)->query();
							$QROW = $QRE->read();
							$response = $dom->createElement("response");
							if ($QROW['value'])
								$response->setAttribute("varName",QueXMLCleanup($QROW['value']));
							$response->appendChild(QueXMLCreateFixed($qid,false,false,0,$other,$sgq));

							//get the header of the second scale of the dual scale question
							$Query = "SELECT value FROM {{question_attributes}} WHERE qid = $qid AND language='$quexmllang' AND attribute='dualscale_headerB'";
							$QRE = Yii::app()->db->createCommand($Query)->query();
							$QROW = $QRE->read();
							$response2 = $dom->createElement("response");
							if ($QROW['value'])
								$response2->setAttribute("varName",QueXMLCleanup($QROW['value']));
							$response2->appendChild(QueXMLCreateFixed($qid,false,false,1,$other,$sgq));
							$question->appendChild($response);
							$question->appendChild($response2);*/
							break;
						case ":": //multi-flexi array numbers
							/*quexml_create_subQuestions($question,$qid,$sgq);
							//get multiflexible_checkbox - if set then each box is a checkbox (single fixed response)
							$mcb  = quexml_get_lengthth($qid,'multiflexible_checkbox',-1);
							if ($mcb != -1)
								quexml_create_multi($question,$qid,$sgq,1);
							else
							{
								//get multiflexible_max and maximum_chars - if set then make boxes of max of these widths
								$mcm = max(quexml_get_lengthth($qid,'maximum_chars',1), strlen(quexml_get_lengthth($qid,'multiflexible_max',1)));
								quexml_create_multi($question,$qid,$sgq,1,array('f' => 'integer', 'len' => $mcm, 'lab' => ''));
							}*/
							break;
						case ";": //multi-flexi array text
							/*quexml_create_subQuestions($question,$qid,$sgq);
							//foreach question where scale_id = 1 this is a textbox
							quexml_create_multi($question,$qid,$sgq,1,array('f' => 'text', 'len' => quexml_get_lengthth($qid,'maximum_chars',10), 'lab' => ''));
							*/
							break;
						case "^": //SLIDER CONTROL - not supported
							/*$response->appendChild(QueXMLFixedArray(array("NOT SUPPORTED:$type" => 1)));
							$question->appendChild($response);
							*/
							break;
					} //End Switch


					// add to questions array
					$result->questions[] = $question;

				}
			}

			return $result;
		}

		public static function getQuestionAnswersAsArray($qid, $rotate=false, $labels=true, $scale=0, $other=false, $varname="")
		{
			global $quexmllang;
			$qlang = new limesurvey_lang($quexmllang);

			if ($labels)
				$Query = "SELECT * FROM {{labels}} WHERE lid = $labels  AND language='$quexmllang' ORDER BY sortorder ASC";
			else
				$Query = "SELECT code,answer as title,sortorder FROM {{answers}} WHERE qid = $qid AND scale_id = $scale  AND language='$quexmllang' ORDER BY sortorder ASC";

			$QueryResult = Yii::app()->db->createCommand($Query)->query();

			$answersArray = [];

			foreach($QueryResult->readAll() as $row)
			{
				$answersArray[$row['code']] = $row['title'];
			}

			return $answersArray;
		}

		public static function getSubquestionsAsArray($qid, $scale_id = false, $free = false, $other = false)
		{
			$questions_array = [];

			global $quexmllang ;
			global $iSurveyID;
			$qlang = new limesurvey_lang($quexmllang);


			$Query = "SELECT * FROM {{questions}} WHERE parent_qid = $qid  AND language='$quexmllang' ";
			if ($scale_id != false) $Query .= " AND scale_id = $scale_id ";
			$Query .= " ORDER BY question_order ASC";
			//$QueryResult = mysql_query($Query) or die ("ERROR: $QueryResult<br />".mysql_error());
			$QueryResult = Yii::app()->db->createCommand($Query)->query();

			$nextcode = "";

			foreach($QueryResult->readAll() as $Row)
			{

				$questions_array[$Row['title']] = $Row['question'];
			}

			if ($other && $free==false)
			{
				// todo: other....
			}




			return $questions_array;

		}


		public static function getSurveyAnswers($survey_id)
		{
			$survey_id = preg_replace('/[^0-9]/', '', $survey_id);
			$tablename = "survey_" . $survey_id;

			$Query = "
SELECT *
FROM $tablename
";
			//$QueryResult = mysql_query($Query) or die ("ERROR: $QueryResult<br />".mysql_error());
			$QueryResult = Yii::app()->db->createCommand($Query)->query();

			// the stupid way, one by one
			$field_results = [];
			$total_answers = 0;
			foreach($QueryResult->readAll() as $row)
			{
				$total_answers++;

//				var_dump($row);

				foreach($row as $fieldname => $val)
				{
					// if data column (starts with id of the survey)
					if(strpos($fieldname, $survey_id) === 0)
					{
						$answer_code = str_replace($survey_id, '', $fieldname);

						if(!isset($field_results[$answer_code]))
						{
							$field_results[$answer_code] = [];
						}
						if(!isset($field_results[$answer_code][$val]))
						{
							$field_results[$answer_code][$val] = 0;
						}

						$field_results[$answer_code][$val]++;
					}
				}
			}

//			var_dump($field_results);

			// now combine with survey structure
			$survey_structure = self::getSurveyAsObject($survey_id);

			// fill answer statistics
			$survey_structure->answers = [];
			foreach($survey_structure->questions as $question)
			{
				$answer = new stdClass();

				$answer->id = $question->id;
				$answer->text = $question->text;
				$answer->results = [];

				foreach($question->options as $answer_code => $answer_text)
				{
					$result = new stdClass();

					$result->text = $answer_text;
					$result->code = $answer_code;

					// @todo: this breaks for text questions

					// not multiple
					if(!$question->multiple)
					{
						// we have to check question values that match the answer code
						$result->votes = isset($field_results[$question->code][$answer_code])
							? $field_results[$question->code][$answer_code]
							: 0;
					}
					else
					{
						// we have to check number of answers having 'Y'
						// note that table field name is composed of question code and answer code
						$table_column_name = $question->code . $answer_code;

						$result->votes = isset($field_results[$table_column_name]['Y'])
							? $field_results[$table_column_name]['Y']
							: 0;
					}


					$answer->results[] = $result;
				}




				// add to answers
				$survey_structure->answers[] = $answer;
			}

			// remove questions
			unset($survey_structure->questions);

			$survey_structure->total_answers = $total_answers;

			return $survey_structure;

		}

		/**
		 * publish a message to redis
		 * returns array ['success' => true/false, 'message' => optional error message]
		 *
		 * @param $channel
		 * @param $message
		 * @return array
		 */
		public static function publishToRedis($channel, $message)
		{
			$return = [
				'success' => true,
				'message' => ''
			];

			try
			{
				// Prepend a base path if Predis is not available in your "include_path".
				require APPPATH . 'composer/vendor/predis/predis/src/Autoloader.php';

				Predis\Autoloader::register();

				$client = new Predis\Client(array(
					'scheme' => 'tcp',
					'host'   => '127.0.0.1',
					'port'   => 6379,
				));

				$client->publish($channel, $message);
			}
			catch(Exception $e)
			{
				$return = [
					'success' => false,
					'message' => $e->getMessage()
				];
			}

			return $return;
		}

	}
